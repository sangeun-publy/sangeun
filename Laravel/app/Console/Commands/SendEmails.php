<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\StandupAlarm;
Use App\User;
Use App\Standup;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an alarm e-mail to users who did not write standup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date("Y-m-d");
        $allUserIds = User::pluck('id');
        $todayStandupUserIds = Standup::where('date', $today)->pluck('user_id');
        $userIdsArrayKV = $allUserIds->diff($todayStandupUserIds)->all();
        $userIdsArray = array_flatten($userIdsArrayKV);

        $users = User::whereIn('id', $userIdsArray)
                        ->get();
                        
        $users->each(function($user){
            \Mail::to($user)->send(new StandupAlarm($user)); 
        });
    }
}
