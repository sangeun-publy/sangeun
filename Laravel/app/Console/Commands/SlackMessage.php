<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class SlackMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a slack message for standup meeting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Slack Message
        \Slack::send('스탠드업 미팅 시간입니다.');       
    }
}
