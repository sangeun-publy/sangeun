<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Standup;
use Carbon\Carbon;

class StandupController extends Controller
{
	public function index(){
		$userId = auth()->id();
		$date = (!request('date'))? Carbon::today() : Carbon::parse(request('date'));
		$oneWeekAgo = $date->copy()->subDays(6)->toDateString();
		$date = $date->toDateString();

		$standups = Standup::where('user_id', $userId)
							->whereBetween('date',[$oneWeekAgo, $date])
							->latest('date')
							->get();

		return view('standup.weekly')->with('date', $date )
									->with('oneWeekAgo', $oneWeekAgo)
									->with(compact('standups'));
	}

	public function create(){
		$userId = auth()->id();
		$date = (!request('date'))? Carbon::today() : Carbon::parse(request('date'));
		$previousDate = $date->copy()->subDay()->toDateString();
		$date = $date->toDateString();

		$todayStandup = Standup::where('user_id', $userId)
							->where('date', $date)
							->first(); 

		$yesterdayDone = Standup::where('user_id', $userId)
							->where('date', $previousDate)
							->value('todo');

		return view('standup.me')->with('date', $date)
								->with('todayStandup', $todayStandup)
								->with('yesterdayDone', $yesterdayDone);
	}

	public function show(Request $request){
		
		$date = (!request('date'))? Carbon::today() : Carbon::parse(request('date'));
		$date = $date->toDateString();
		$standups = Standup::where('date', $date)->get(); 

		return view('standup.all')->with('date', $date)
									->with(compact('standups'));

	}

	public function store(){
		$this->validate(request(), [
			'todo' => 'required'
		]);
		     
		$date = (! request('date')) ? Carbon::today() : Carbon::parse(request('date'));
		$date = $date->toDateString();
		$userId = auth()->id();

		if(Standup::where('user_id', $userId)
					->where('date', $date)->count()){
			Standup::where('user_id', $userId)
					->where('date', $date)
					->update(['done' => request('done'),
							'todo' => request('todo'),
							'hard' => request('hard')]);
		}

		else{
			Standup::create([
				'done' => request('done'),
				'todo' => request('todo'),
				'hard' => request('hard'),
				'date' => $date,
				'user_id' => $userId
			]);
		}
		return redirect('me'."?date=".$date);
	}
}
