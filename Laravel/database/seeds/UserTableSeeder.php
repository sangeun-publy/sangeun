<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       User::insert([
            [
                'name' => 'Scarlett',
                'email' => 'scarlett@ex.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Lettie',
                'email' => 'scarlett.lee.git@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Abby',
                'email' => 'abby@ex.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Sangeun',
                'email' => 'sangeun@publy.co',
                'password' => bcrypt('secret'),
            ],
        ]);

   }
}
