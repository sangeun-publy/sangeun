<?php

use Illuminate\Database\Seeder;
use App\Standup;

class StandupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Standup::insert([
            [
                'user_id' => 3,
                'done' => 'Make ME page',
                'todo' => 'Make ALL Page',
                'hard' => 'calender',
                'date' => '2017-08-22',
            ],
            [
                'user_id' => 1,
                'done' => 'Blog ex',
                'todo' => 'Make a site',
                'hard' => 'database',
                'date' => '2017-08-22',
            ],
        ]);
    }
}
