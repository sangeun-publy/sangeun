<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StandupController@show');
Route::get('/home', 'StandupController@show')->name('home');

Route::get('/me', 'StandupController@create')->name('me')->middleware('auth');
Route::post('/me', 'StandupController@store');
Route::get('/me/weekly', 'StandupController@index')->name('weekly')->middleware('auth');

Auth::routes();
