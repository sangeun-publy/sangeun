@extends('layouts.master')

@section('content')
	<h1 class="blog-title"> Daily </h1>
	@include('layouts.calendar1', ['startDate' => $date, 'endDate' => $date])
	<div class="col-sm-7 blog-main">
		
		<form method="POST" action="/me">
			{{ csrf_field() }}
			<input type="hidden" name="date" value={{ $date }}>

			<div class="form-group">
				<label for="done">DONE</label>
				<textarea class="form-control" id="done" name="done" rows="3">@if( $yesterdayDone ) {{ $yesterdayDone }}&#13;&#10;@endif @if( $todayStandup ){{$todayStandup->done }}@endif</textarea>
			</div>

			<div class="form-group">
				<label for="todo">TO DO</label>
				<textarea class="form-control" id="todo" name="todo" rows="3">@if ( $todayStandup ) {{ $todayStandup->todo }}@endif</textarea>
			</div>

			<div class="form-group">
				<label for="hard">HARD THINGS</label>
				<textarea class="form-control" id="hard" name="hard" rows="3">@if ( $todayStandup ) {{ $todayStandup->hard }}@endif</textarea>
			</div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			@include('layouts.errors')
		</form>


	</div>
	@include('layouts.calendar2')

@endsection