@extends('layouts.master')

@section('content')
	<h1 class="blog-title"> Teams </h1>
	@include('layouts.calendar1', ['startDate' => $date, 'endDate' => $date])

	<div class="col-sm-7 blog-main">
		@foreach($standups as $standup)
			<div class="blog-post-meta"> {{ $standup->user->name }} </div>
			@include ('layouts.standup')
		@endforeach

	</div><!-- /.blog-main -->
	@include('layouts.calendar2')

@endsection