@extends('layouts.master')

@section('content')
	<h1 class="blog-title"> Weekly </h1>
	@include('layouts.calendar1', ['startDate' => $oneWeekAgo, 'endDate' => $date])
	<div class="col-sm-7 blog-main">
		@foreach($standups as $standup)
			{{ Carbon\Carbon::parse($standup->date)->format('D, m-d-Y') }}
			@include ('layouts.standup')
		@endforeach
	</div>
	@include('layouts.calendar2')
@endsection