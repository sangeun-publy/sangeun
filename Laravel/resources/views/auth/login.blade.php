@extends ('layouts.master')

@section('content')
<div class="col-md-7">
    <div class="blog-header"> 
        <h1 class="blog-title">Login</h1>
    </div>

    <form method="POST" action="{{ route('login') }}">
        {{csrf_field()}}

        <div class="form-group">
            <label for="email">Email Address</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Login</button>
            <a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
        </div>

        @include ('layouts.errors')

    </form>


</div>
@endsection