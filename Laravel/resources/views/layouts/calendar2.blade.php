<div class="col-sm-4 offset-sm-1 blog-sidebar">
	<div class="sidebar-module sidebar-module-inset">
		<div id="calendar"></div>

	
		<script type="text/javascript">
			function parseDate(input) {
				var parts = input.split("-");
				return new Date(parts[0], parts[1]-1, parts[2]);
			};
				 
			var calendar = $('#calendar').datepicker({
				//todayHighlight: true
			})
			.datepicker('setDate', parseDate( "{{ $date }}" ))
			.on('changeDate', function (e) {
				var yy = e.date.getFullYear();
				var mm = e.date.getMonth()+1;
				var dd = e.date.getDate();
				
				var date = [yy, '-', 
				(mm>9 ? '' : '0') + mm, '-',
				(dd>9 ? '' : '0') + dd
				].join('');
				
				var param = { date : date };
				var str = jQuery.param( param );
				window.location.replace("?"+str);
			});
		</script>
	</div>


</div>