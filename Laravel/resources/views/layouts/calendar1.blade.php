<div class="blog-header"> 
	<div class="container">			
		<h1>
			@if($startDate == $endDate)
				{{ Carbon\Carbon::parse($startDate)->format('D, m-d-Y') }}
			@else
				{{ Carbon\Carbon::parse($startDate)->format('D, m-d-Y')." ~ ".Carbon\Carbon::parse($endDate)->format('D, m-d-Y')}}
			@endif 
		</h1>
	</div>
</div>

