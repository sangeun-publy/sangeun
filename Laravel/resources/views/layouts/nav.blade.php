{{-- <script type="text/javascript">
	$(".nav a").click(function(){
		$(".nav-link").removeClass("active");
		$(this).addClass("active");
	});
</script> --}}

<div class="blog-masthead">
	<div class="container">
		<nav class="nav blog-nav">
			<a class="nav-link" href="{{ route('home') }}">Teams</a>
			<a class="nav-link" href="{{ route('me') }}">Daily</a>
			<a class="nav-link" href="{{ route('weekly') }}">Weekly</a>

			@if (Auth::check())
				<div class="dropdown ml-auto">
					<button class="btn btn-link btn-lg dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						{{ Auth::user()->name }}
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						<button class="dropdown-item" type="button" href="{{ route('logout') }}"
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
							logout</button>	

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>		
					</div>
				</div>

			@else
				<a class="nav-link ml-auto" href="{{ route('register') }}">REGISTER</a>
				<a class="nav-link" href="{{ route('login') }}">LOGIN</a>
			@endif
		</nav>
	</div>
</div>