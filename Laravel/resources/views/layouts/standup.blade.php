<div class="blog-post">

	<div class="container" id="standup">

		<h2 class="blog-post-title"> DONE </h2>		
		<p class="indent"> {!! nl2br($standup->done) !!} </p>
		<br>

		<h2 class="blog-post-title"> TO DO </h2>
		<p class="indent"> {!! nl2br($standup->todo) !!} </p>
		<br>

		<h2 class="blog-post-title"> HARD </h2>
		<p class="indent"> {!! nl2br($standup->hard) !!} </p>
	</div>

</div>